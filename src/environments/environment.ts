// The file contents for the current environment will overwrite these during
// build. The build system defaults to the dev environment which uses
// `environment.ts`, but if you do `ng build --env=prod` then
// `environment.prod.ts` will be used instead. The list of which env maps to
// which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC8EDSAhU2YUWijY8gFxe_bzzjhQCAG71I',
    authDomain: 'admin-siscom.firebaseapp.com',
    databaseURL: 'https://admin-siscom.firebaseio.com',
    projectId: 'admin-siscom',
    storageBucket: 'admin-siscom.appspot.com',
    messagingSenderId: '877807973024'
  }
};
