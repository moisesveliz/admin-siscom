import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {firestore} from 'firebase/app';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Rx';

import {CommonConstants} from '../../core/util/common-constants';
import {ComponentsSupport} from '../../core/util/components-support';
import {IMessage} from '../model/i-message';
import {IOrder} from '../model/i-order';
import {IOrderId} from '../model/i-order-id';
import {Page} from '../model/page';
import {PagedData} from '../model/paged-data';
import {StandartMessage} from '../model/standart-message';

@Injectable()
export class OrderService {
  private ordersCollection: AngularFirestoreCollection<IOrder>;
  private orderIds$: Observable<IOrderId[]>;
  private page$: Observable<Page>;
  private sizeFilter$: BehaviorSubject<number|null>;
  private pageNumberFilter$: BehaviorSubject<number|null>;
  private sortedAllOrders$: Observable<IOrder[]>;

  constructor(private readonly afs: AngularFirestore) {
    this.sizeFilter$ = new BehaviorSubject(null);
    this.pageNumberFilter$ = new BehaviorSubject(null);
    this.ordersCollection = afs.collection<IOrder>('orders');
    this.loadSortedAllOrders();
    this.loadOrderIds();
  }
  private loadSortedAllOrders() {
    this.sortedAllOrders$ = this.ordersCollection.valueChanges().map(
        data => data.sort((a: IOrderId, b: IOrderId) => {
          if (a.id < b.id) {
            return -1;
          }
          if (a.id > b.id) {
            return 1;
          }
          return 0;
        }));
  }
  private loadOrderIds() {
    this.orderIds$ =
        Observable
            .combineLatest(
                this.sizeFilter$, this.pageNumberFilter$, this.sortedAllOrders$)
            .switchMap(([size, pageNumber = 0, allOrders]) => {
              return this.afs
                  .collection<IOrder>(
                      'orders',
                      ref => {
                        const query: firestore.CollectionReference|
                            firestore.Query = ref;
                        const lastElement = pageNumber * 20;
                        const lastPageOrder = allOrders[lastElement];
                        return query.orderBy('id', 'asc')
                            .startAt(lastPageOrder.id)
                            .limit(20);
                      })
                  .stateChanges(['added', 'modified', 'removed'])
                  .map(actions => {
                    return actions.map(a => {
                      const data = a.payload.doc.data() as IOrder;
                      const documentId = a.payload.doc.id;
                      return {documentId, ...data} as IOrderId;
                    });
                  });
            });
  }
  private filterBySize(size: number|null) {
    this.sizeFilter$.next(size);
  }
  private filterByPageNumber(pageNumber: number|null) {
    this.pageNumberFilter$.next(pageNumber);
  }
  all(): Observable<IOrderId[]> {
    return this.orderIds$;
  }
  filterByPaging(page: Page): Promise<Page> {
    this.filterBySize(page.size);
    this.filterByPageNumber(page.pageNumber);
    return this.getPagingOfOrders(page.size, page.pageNumber);
  }
  getPagingOfOrders(size, pageNumber): Promise<Page> {
    return new Promise((resolve, reject) => {
      this.ordersCollection.valueChanges()
          .map(orders => {
            const totalPages = Math.round(orders.length / size);
            const totalElements = orders.length;
            const page = {size, pageNumber, totalElements, totalPages};
            return page;
          })
          .toPromise()
          .then(page => resolve(page))
          .catch(error => reject(error));
    });
  }
  add(params: IOrder): Observable<IMessage> {
    const message = new StandartMessage(
        CommonConstants.SUCCESS, 'Registrado satisfactoriamente');
    const id = this.afs.createId();
    const order: IOrder = {
      id: id,
      date: params.date,
      amount: params.amount,
      sustention: params.sustention,
      product: params.product
    };
    return Observable.from(this.ordersCollection.add(order))
        .map((x) => {
          return message;
        })
        .catch((error) => {
          message.code = CommonConstants.DANGER;
          message.message = error;
          return Observable.throw(message);
        });
  }
  update(params: IOrderId): Observable<IMessage> {
    const message = new StandartMessage(
        CommonConstants.SUCCESS, 'Actualizado satisfactoriamente');
    const id = this.afs.createId();
    const order: IOrder = {
      date: params.date,
      amount: params.amount,
      sustention: params.sustention,
      product: params.product
    };
    return Observable
        .from(this.afs.collection<IOrder>(`orders`)
                  .doc(params.documentId)
                  .update(order))
        .map((x) => {
          return message;
        })
        .catch((error) => {
          message.code = CommonConstants.DANGER;
          message.message = error;
          return Observable.throw(message);
        });
  }
  delete(params: IOrderId): Observable<IMessage> {
    const message = new StandartMessage(
        CommonConstants.SUCCESS, 'Eliminado satisfactoriamente');
    return Observable
        .from(this.afs.collection<IOrder>(`orders`)
                  .doc(params.documentId)
                  .delete())
        .map((data) => {
          return message;
        })
        .catch((error) => {
          message.code = CommonConstants.DANGER;
          message.message = error;
          return Observable.throw(message);
        });
  }
  get(documentId: string): Observable<IOrderId> {
    return this.afs.doc<IOrderId>(`orders/${documentId}`).valueChanges();
  }
}
