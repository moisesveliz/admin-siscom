import {inject, TestBed} from '@angular/core/testing';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFirestoreModule} from 'angularfire2/firestore';

import {environment} from '../../../environments/environment';

import {OrderService} from './order.service';

describe('OrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderService],
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule, AngularFireAuthModule
      ]
    });
  });

  it('should be created', inject([OrderService], (service: OrderService) => {
       expect(service).toBeTruthy();
     }));
});
