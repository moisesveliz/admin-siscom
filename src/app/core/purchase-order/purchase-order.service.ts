import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

import {CommonConstants} from '../../core/util/common-constants';
import {ValidationSupport} from '../../core/util/validation-support';
import {ControlBase} from '../model/control-base';
import {DateControl} from '../model/date-control';
import {DropdownControl} from '../model/dropdown-control';
import {TextareaControl} from '../model/textarea-control';
import {TextboxControl} from '../model/textbox-control';
import {ComponentsSupport} from '../util/components-support';

@Injectable()
export class PurchaseOrderService {
  constructor(private http: HttpClient) {}

  getControlsPurchaseOrder(): Observable<ControlBase<any>[]> {
    return this.http.get(CommonConstants.PURCHASE_ORDER_CONTROL_API)
        .map(
            (resp: any) =>
                ComponentsSupport.getCustomControls(resp.purchaseOrderControl))
        .catch((error: any) => Observable.throw(error.json()));
  }
}
