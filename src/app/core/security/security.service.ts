import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {CommonConstants} from '../../core/util/common-constants';
import {OptionGroup} from '../model/option-group';

@Injectable()
export class SecurityService {
  constructor(private http: HttpClient) {}

  getMenu(): Observable<OptionGroup[]> {
    return this.http.get(CommonConstants.MENU_API)
        .map((resp: any) => resp.menu)
        .catch((error: any) => Observable.throw(error.json()));
  }
}
