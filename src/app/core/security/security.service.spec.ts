import {inject, TestBed} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {SecurityService} from './security.service';

describe('SecurityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule(
        {providers: [SecurityService], imports: [HttpModule]});
  });

  it('should be created',
     inject([SecurityService], (service: SecurityService) => {
       expect(service).toBeTruthy();
     }));
});
