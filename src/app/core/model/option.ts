export class Option {
  img: string;
  icon: string;
  name: string;
  desc: string;
  active: boolean;
  link: string;
  childs: Array<Option>;
}
