export class ControlBase<T> {
  value: T;
  key: string;
  label: string;
  order: number;
  controlType: string;
  validators: Array<any>;

  constructor(options: {
    value?: T,
    key?: string,
    label?: string,
    order?: number,
    controlType?: string,
    validators?: Array<any>
  } = {}) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || '';
    this.validators = options.validators || [];
  }
}
