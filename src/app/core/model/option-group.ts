import {Option} from './option';

export class OptionGroup {
  grp: string;
  ord: number;
  type: string;
  opts: Array<Option>;
}
