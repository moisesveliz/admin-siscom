export class Lang {
  name: string;
  icon: string;
  value: string;
}
