export interface IMessage {
  code: string;
  message: string;
  detail?: string;

  getTitle(): string;
  getTypeAlert(): string;
}
