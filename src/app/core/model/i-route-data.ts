export interface IRouteData {
  title?: string;
  action?: string;
}
