import {ControlBase} from './control-base';

export class TextboxControl extends ControlBase<string> {
  controlType = 'textbox';
  placeholder: string;
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
    this.placeholder = options['placeholder'] || '';
  }
}
