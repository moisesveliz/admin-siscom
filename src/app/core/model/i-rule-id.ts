import {IRule} from './i-rule';

export interface IRuleId extends IRule { documentId?: string; }
