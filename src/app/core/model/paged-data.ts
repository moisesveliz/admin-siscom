import {Observable} from 'rxjs/Observable';
import {Page} from './page';

export class PagedData<T> {
  constructor(data: Observable<T[]> = new Observable<T[]>()) {
    this.data = data;
  }
  data = new Observable<T[]>();
  page = new Page();
}
