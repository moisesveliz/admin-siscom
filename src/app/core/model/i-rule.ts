export interface IRule {
  id?: string;
  commerceId: string;
  ruleName: string;
  ruleStatusId: string;
  description: string;
  rules: Array<IRuleDetail>;
}

export interface IRuleDetail {
  field: string;
  condition: string;
  value: string;
  secondaryValue: string;
  label: string;
}
