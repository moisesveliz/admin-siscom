import {ControlBase} from './control-base';

export class DateControl extends ControlBase<string> {
  controlType = 'date';
  placeholder: string;
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
    this.placeholder = options['placeholder'] || '';
  }
}
