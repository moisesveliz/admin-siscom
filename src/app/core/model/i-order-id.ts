import {IOrder} from './i-order';

export interface IOrderId extends IOrder { documentId?: string; }
