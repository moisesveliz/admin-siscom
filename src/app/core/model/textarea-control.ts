import {ControlBase} from './control-base';

export class TextareaControl extends ControlBase<string> {
  controlType = 'textarea';
  placeholder: string;
  type: string;
  rows: number;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
    this.placeholder = options['placeholder'] || '';
    this.rows = options['rows'] || 3;
  }
}
