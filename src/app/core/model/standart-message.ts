import {CommonConstants} from '../../core/util/common-constants';
import {IMessage} from './i-message';

export class StandartMessage implements IMessage {
  code: string;
  message: string;
  detail?: string;
  constructor(code: string, message: string, detail?: string) {
    this.code = code;
    this.message = message;
    this.detail = detail;
  }
  getTitle(): string {
    switch (this.code) {
      case CommonConstants.SUCCESS:
        return CommonConstants.TITLE_INFO;
      case CommonConstants.WARNING:
        return CommonConstants.TITLE_INFO;
      case CommonConstants.DANGER:
        return CommonConstants.TITLE_ERROR;
      default:
        return CommonConstants.TITLE_ERROR;
    }
  }
  getTypeAlert(): string {
    switch (this.code) {
      case CommonConstants.SUCCESS:
        return CommonConstants.TOAST_SUCCESS;
      case CommonConstants.WARNING:
        return CommonConstants.TOAST_INFO;
      case CommonConstants.DANGER:
        return CommonConstants.TOAST_ERROR;
      default:
        return CommonConstants.TOAST_ERROR;
    }
  }
}
