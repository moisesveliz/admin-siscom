import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {ControlBase} from '../model/control-base';

@Injectable()
export class DynamicControlService {
  constructor() {}

  toFormGroup(controls: ControlBase<any>[]) {
    const group: any = {};
    controls.forEach(control => {
      group[control.key] = control.validators ?
          new FormControl(control.value || '', control.validators) :
          new FormControl(control.value || '');
    });
    return new FormGroup(group);
  }
}
