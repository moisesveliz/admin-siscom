import { TestBed, inject } from '@angular/core/testing';

import { DynamicControlService } from './dynamic-control.service';

describe('DynamicControlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DynamicControlService]
    });
  });

  it('should be created', inject([DynamicControlService], (service: DynamicControlService) => {
    expect(service).toBeTruthy();
  }));
});
