import {inject, TestBed} from '@angular/core/testing';
import {ToastrModule, ToastrService} from 'ngx-toastr';

import {CommonService} from './common.service';

describe('CommonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule(
        {providers: [CommonService], imports: [ToastrModule.forRoot()]});
  });

  it('should be created', inject([CommonService], (service: CommonService) => {
       expect(service).toBeTruthy();
     }));
});
