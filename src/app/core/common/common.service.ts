import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {IMessage} from '../model/i-message';

@Injectable()
export class CommonService {
  constructor(private toastr: ToastrService) {}

  showMessage(iMessage: IMessage) {
    this.toastr.show(
        iMessage.message, iMessage.getTitle(),
        {timeOut: 5000, positionClass: 'toast-center-center'},
        iMessage.getTypeAlert());
  }
}
