import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MAT_LABEL_GLOBAL_OPTIONS} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ToastrModule} from 'ngx-toastr';

import {NgbDateMomentParserFormatter} from '../core/util/ngb-date-moment-parser-formatter';

import {BusinessBaseService} from './business-base/business-base.service';
import {CommerceService} from './commerce/commerce.service';
import {CommonService} from './common/common.service';
import {DynamicControlService} from './dynamic-control/dynamic-control.service';
import {OrderService} from './order/order.service';
import {PurchaseOrderService} from './purchase-order/purchase-order.service';
import {RuleService} from './rule/rule.service';
import {SecurityService} from './security/security.service';

@NgModule({
  imports: [CommonModule, BrowserAnimationsModule, ToastrModule.forRoot()],
  declarations: [],
  providers: [
    BusinessBaseService, OrderService, CommonService, SecurityService,
    DynamicControlService, PurchaseOrderService, RuleService, CommerceService,
    {provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: {float: 'auto'}}
  ]
})
export class CoreModule {}
