import {FormControl} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';

export class FunctionalSupport {
  customErrorStateMatcher: ErrorStateMatcher = {
    isErrorState: (control: FormControl|null) => {
      if (control) {
        const hasInteraction = control.dirty || control.touched;
        const isInvalid = control.invalid;
        return !!(hasInteraction && isInvalid);
      }
      return false;
    }
  };
}
