export class CommonConstants {
  static PRODUCTS_API = './assets/data/products.json';
  static FIELD_TYPES_BY_ACQUIRER_API =
      './assets/data/field-types-by-acquirer.json';
  static OPERATORS_BY_ACQUIRER_API = './assets/data/operators-by-acquirer.json';
  static COMMERCES_BY_ACQUIRER_API = './assets/data/commerces-by-acquirer.json';
  static PARAMS_CHILDREN_BY_MNEMONIC_PARENT_PARAM =
      './assets/data/params-children-by-mnemonic-parent-param.json';
  static VARIABLE_FIELDS_BY_ACQUIRER_API =
      './assets/data/variable-fields-by-acquirer.json';
  static FUNCTION_FIELDS_BY_ACQUIRER_API =
      './assets/data/function-fields-by-acquirer.json';
  static VALUE_TAG_AND_VALIDATOR_BY_FIELD_AND_ACQUIRER_API =
      './assets/data/value-tag-and-validator-by-field-and-acquirer.json';
  static CONDITIONS_BY_FIELD_BY_ACQUIRER_API =
      './assets/data/conditions-by-field-by-acquirer.json';
  static PURCHASE_ORDER_CONTROL_API =
      './assets/data/purchase-order-control.json';
  static MENU_API = './assets/data/menu.json';
  static ACTION_REGISTER = 'register';
  static ACTION_UPDATE = 'update';
  static EMPTY_VALUE = '';
  static ALL_VALUE = '-1';
  static NONE_VALUE = '-2';
  static SELECT_VALUE = '-3';
  static SUCCESS = '0';
  static WARNING = '4';
  static DANGER = '8';
  static TOAST_SUCCESS = 'toast-success';
  static TOAST_INFO = 'toast-warning';
  static TOAST_ERROR = 'toast-error';
  static TITLE_SAVE = 'Guardar';
  static TITLE_REGISTER = 'Registrar';
  static TITLE_UPDATED = 'Actualizar';
  static TITLE_ERROR = 'Error';
  static TITLE_INFO = 'Información';
  static FORMAT_SHORT_DDMMYYYY = 'DDMMYYYY';
  static FORMAT_LARGE_DDMMYYYY = 'DD-MM-YYYY';

  static VALIDATOR_REQUIRED = 'required';
  static VALIDATOR_INVALID_CREDIT_CARD = 'invalidCreditCard';
  static VALIDATOR_INVALID_EMAIL_ADDRESS = 'invalidEmailAddress';
  static VALIDATOR_INVALID_PASSWORD = 'invalidPassword';
  static VALIDATOR_INVALID_NUMBER = 'invalidNumber';
  static VALIDATOR_INVALID_AMOUNT = 'invalidAmount';
  static VALIDATOR_MIN_LENGTH = 'minlength';
  static VALIDATOR_INVALID_NOT_EMPTY_STRING = 'invalidNotEmptyString';
  static VALIDATOR_INVALID_SELECTED_ITEM = 'invalidSelectedItem';
  static VALIDATOR_INVALID_NOT_EMPTY_DATE = 'invalidNotEmptyDate';

  static FIELD_TYPE_FUNCTION = 'function';
  static FIELD_TYPE_VARIABLE = 'variable';

  static MNEMONIC_PARENT_PARAM_RULE_STATUS = 'RULE_STATUS';
}
