import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {isNumber} from '@ng-bootstrap/ng-bootstrap/util/util';
import {isDate, isObject} from 'util';
import {ILang} from '../../core/model/i-lang';
import {CommonConstants} from './common-constants';

export class ValidationSupport {
  static getValidatorErrorMessage(validatorName: string, validatorValue?: any):
      ILang {
    const config = {
      'required': {name: 'GENERAL.VALIDATION.REQUIRED'},
      'invalidCreditCard': {name: 'GENERAL.VALIDATION.INVALID_CREDIT_CARD'},
      'invalidEmailAddress': {name: 'GENERAL.VALIDATION.INVALID_EMAIL_ADDRESS'},
      'invalidPassword': {name: 'GENERAL.VALIDATION.INVALID_PASSWORD'},
      'invalidNumber': {name: 'GENERAL.VALIDATION.INVALID_NUMBER'},
      'invalidAmount': {name: 'GENERAL.VALIDATION.INVALID_AMOUNT'},
      'minlength': {
        name: 'MIN_LENGTH',
        values: {required_length: validatorValue.requiredLength}
      } as ILang,
      'invalidNotEmptyString':
          {name: 'GENERAL.VALIDATION.INVALID_NOT_EMPTY_STRING'},
      'invalidSelectedItem': {name: 'GENERAL.VALIDATION.INVALID_SELECTED_ITEM'},
      'invalidNotEmptyDate':
          {name: 'GENERAL.VALIDATION.INVALID_NOT_EMPTY_DATE'},
      'invalidNotItemInArray':
          {name: 'GENERAL.VALIDATION.INVALID_NOT_ITEM_IN_ARRAY'}
    };
    return config[validatorName];
  }
  static creditCardValidator(control) {
    // Visa, MasterCard, American Express, Diners Club, Discover, JCB
    if (control.value &&
        control.value.match(
            // tslint:disable-next-line:max-line-length
            /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
      return null;
    } else {
      return {'invalidCreditCard': true};
    }
  }
  static emailValidator(control) {
    // RFC 2822 compliant regex
    if (control.value &&
        control.value.match(
            // tslint:disable-next-line:max-line-length
            /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
      return null;
    } else {
      return {'invalidEmailAddress': true};
    }
  }
  static passwordValidator(control) {
    // {6,100}           - Assert password is between 6 and 100 characters
    // (?=.*[0-9])       - Assert a string has at least one number
    if (control.value &&
        control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
      return null;
    } else {
      return {'invalidPassword': true};
    }
  }
  static numberValidator(control) {
    if (control.value && control.value.match(/^(0|[1-9][0-9]*)$/)) {
      return null;
    } else {
      return {'invalidNumber': true};
    }
  }
  static amountValidator(control) {
    if (control.value && ValidationSupport.amountMatch(control.value)) {
      return null;
    } else {
      return {'invalidAmount': true};
    }
  }
  static amountValidatorPromise(control): Promise<any> {
    return new Promise((resolve, reject) => {
      if (control.value && ValidationSupport.amountMatch(control.value)) {
        resolve(null);
      } else {
        resolve({'invalidAmount': true});
      }
    });
  }
  static notEmptyStringValidator(control) {
    if (control.value && control.value.length > 0) {
      return null;
    } else {
      return {'invalidNotEmptyString': true};
    }
  }
  static selectedItemValidator(control) {
    if (control.value && control.value !== CommonConstants.EMPTY_VALUE) {
      return null;
    } else {
      return {'invalidSelectedItem': true};
    }
  }
  static notItemInArrayValidator(control) {
    if (control.value.length > 0) {
      return null;
    } else {
      return {'invalidNotItemInArray': true};
    }
  }
  static notEmptyDateValidator(control) {
    if (isObject(control.value) && control.value.hasOwnProperty('day') &&
        control.value.hasOwnProperty('month') &&
        control.value.hasOwnProperty('year') && isNumber(control.value.day) &&
        isNumber(control.value.month) && isNumber(control.value.year) &&
        isDate(new Date(
            Number(control.value.year), Number(control.value.month),
            Number(control.value.day)))) {
      return null;
    } else {
      return {'invalidNotEmptyDate': true};
    }
  }

  private static amountMatch(value): boolean {
    return value.match(/^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$/);
  }
}
