import {Validators} from '@angular/forms';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

import {ControlBase} from '../../core/model/control-base';
import {DateControl} from '../../core/model/date-control';
import {DropdownControl} from '../../core/model/dropdown-control';
import {TextareaControl} from '../../core/model/textarea-control';
import {TextboxControl} from '../../core/model/textbox-control';

import {CommonConstants} from './common-constants';
import {ValidationSupport} from './validation-support';

export class ComponentsSupport {
  static getTodayNgbDateStruct(): NgbDateStruct {
    const my = new Date();
    return {
      year: my.getFullYear(),
      month: my.getMonth() + 1,
      day: my.getDate()
    };
  }
  static convertStringDateToNgbDateStruct(date: string, format: string):
      NgbDateStruct {
    if (date && format === CommonConstants.FORMAT_LARGE_DDMMYYYY) {
      return {
        year: Number(date.slice(6, 11)),
        month: Number(date.slice(3, 5)),
        day: Number(date.slice(0, 2))
      };
    }
    return null;
  }
  static getValidatorsFromArrStrValidators(validators: Array<any>): Array<any> {
    return validators
        .map(validator => {
          const key = validator.key || validator;
          return this.getValidatorFromKey(key);
        })
        .filter(validator => validator !== null);
  }
  static getCustomControls(controls: ControlBase<any>[]): ControlBase<any>[] {
    return controls
        .map(control => {
          control.validators =
              ComponentsSupport.getValidatorsFromArrStrValidators(
                  control.validators);
          return control;
        })
        .map(control => {
          switch (control.controlType) {
            case 'dropdown':
              return new DropdownControl(control);
            case 'textbox':
              return new TextboxControl(control);
            case 'textarea':
              return new TextareaControl(control);
            case 'date':
              return new DateControl(control);
            default:
              return null;
          }
        })
        .filter(control => control !== null)
        .sort((a, b) => a.order - b.order);
  }
  private static getValidatorFromKey(validator) {
    switch (validator) {
      case CommonConstants.VALIDATOR_INVALID_CREDIT_CARD:
        return ValidationSupport.creditCardValidator;
      case CommonConstants.VALIDATOR_INVALID_EMAIL_ADDRESS:
        return ValidationSupport.emailValidator;
      case CommonConstants.VALIDATOR_INVALID_PASSWORD:
        return ValidationSupport.passwordValidator;
      case CommonConstants.VALIDATOR_INVALID_NUMBER:
        return ValidationSupport.numberValidator;
      case CommonConstants.VALIDATOR_INVALID_AMOUNT:
        return ValidationSupport.amountValidator;
      case CommonConstants.VALIDATOR_REQUIRED:
        return Validators.required;
      case CommonConstants.VALIDATOR_MIN_LENGTH:
        return Validators.minLength(Number(validator.value));
      case CommonConstants.VALIDATOR_INVALID_NOT_EMPTY_STRING:
        return ValidationSupport.notEmptyStringValidator;
      case CommonConstants.VALIDATOR_INVALID_SELECTED_ITEM:
        return ValidationSupport.selectedItemValidator;
      case CommonConstants.VALIDATOR_INVALID_NOT_EMPTY_DATE:
        return ValidationSupport.notEmptyDateValidator;
      default:
        return null;
    }
  }
  static addNoneOption(): any {
    return {label: 'Ninguno', value: CommonConstants.NONE_VALUE};
  }
  static addSelectOption(): any {
    return {label: 'Seleccione...', value: CommonConstants.EMPTY_VALUE};
  }
  static addAllOption(): any {
    return {label: 'Todos', value: CommonConstants.ALL_VALUE};
  }
}
