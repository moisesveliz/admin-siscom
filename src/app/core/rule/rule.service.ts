import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {MatPaginator} from '@angular/material';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {firestore} from 'firebase/app';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';

import {ComponentsSupport} from '../../core/util/components-support';
import {IMessage} from '../model/i-message';
import {IRule} from '../model/i-rule';
import {IRuleId} from '../model/i-rule-id';
import {Page} from '../model/page';
import {PagedData} from '../model/paged-data';
import {StandartMessage} from '../model/standart-message';
import {CommonConstants} from '../util/common-constants';

@Injectable()
export class RuleService {
  private rulesCollection: AngularFirestoreCollection<IRule>;
  private ruleIds$: Observable<IRuleId[]>;
  private page$: Observable<Page>;
  private sizeFilter$: BehaviorSubject<number|null>;
  private pageNumberFilter$: BehaviorSubject<number|null>;
  private sortedAllRules$: Observable<IRule[]>;

  constructor(
      private http: HttpClient, private readonly afs: AngularFirestore) {
    this.sizeFilter$ = new BehaviorSubject(null);
    this.pageNumberFilter$ = new BehaviorSubject(null);
    this.rulesCollection = afs.collection<IRule>('rules');
    this.loadSortedAllRules();
    this.loadRuleIds();
  }

  getFieldTypesByAcquirer(params: any): Observable<any[]> {
    return this.http.get(CommonConstants.FIELD_TYPES_BY_ACQUIRER_API)
        .map((resp: any) => resp.fieldTypesByAcquirer)
        .catch((error: any) => Observable.throw(error));
  }
  getOperatorsByAcquirer(params: any): Observable<any[]> {
    return this.http.get(CommonConstants.OPERATORS_BY_ACQUIRER_API)
        .map((resp: any) => resp.operatorsByAcquirer)
        .catch((error: any) => Observable.throw(error));
  }
  getConditionsByFieldByAcquirer(params: any): Observable<any[]> {
    return this.http.get(CommonConstants.CONDITIONS_BY_FIELD_BY_ACQUIRER_API)
        .map((resp: any) => {
          return resp.conditionsByFieldByAcquirer.filter(
              x => x.hasOwnProperty(params.field));
        })
        .map((resp: any) => resp[0][params.field])
        .catch((error: any) => Observable.throw(error));
  }
  getVariableFieldsByAcquirer(params: any): Observable<any[]> {
    return this.http.get(CommonConstants.VARIABLE_FIELDS_BY_ACQUIRER_API)
        .map((resp: any) => resp.variableFieldsByAcquirer)
        .catch((error: any) => Observable.throw(error));
  }
  getFunctionFieldsByAcquirer(params: any): Observable<any[]> {
    return this.http.get(CommonConstants.FUNCTION_FIELDS_BY_ACQUIRER_API)
        .map((resp: any) => resp.functionFieldsByAcquirer)
        .catch((error: any) => Observable.throw(error));
  }
  getValueTagAndValidatorByFieldAndAcquirer(params: any): Observable<any[]> {
    return this.http
        .get(CommonConstants.VALUE_TAG_AND_VALIDATOR_BY_FIELD_AND_ACQUIRER_API)
        .map(
            (resp: any) => resp.valueTagAndValidatorByFieldAndAcquirer.filter(
                x => x.hasOwnProperty(params.field)))
        .map((resp: any) => resp[0][params.field])
        .catch((error: any) => Observable.throw(error));
  }
  validateAndDiscardRuleFields(arrVarFuncFieldsByAcquirer: any[], rules: any[]):
      any[] {
    return arrVarFuncFieldsByAcquirer.filter(field => {
      const foundElement = rules.find(rule => {
        const fieldInMaster = field.value;
        const fieldInRules = rule.field;
        const markedAsGreen = 'markedAsGreen';
        const markedAsBlack = 'markedAsBlack';
        const vci = 'VCI';
        if (fieldInMaster === markedAsBlack && fieldInRules === markedAsGreen) {
          return true;
        }
        if (fieldInMaster === markedAsGreen && fieldInRules === markedAsBlack) {
          return true;
        }
        if (fieldInMaster === vci) {
          return false;
        }
        return fieldInRules === fieldInMaster;
      });
      return !foundElement;
    });
  }
  // Dummy
  private loadSortedAllRules() {
    this.sortedAllRules$ = this.rulesCollection.valueChanges().map(
        data => data.sort((a: IRuleId, b: IRuleId) => {
          if (a.id < b.id) {
            return -1;
          }
          if (a.id > b.id) {
            return 1;
          }
          return 0;
        }));
  }
  private loadRuleIds() {
    this.ruleIds$ =
        Observable
            .combineLatest(
                this.sizeFilter$, this.pageNumberFilter$, this.sortedAllRules$)
            .switchMap(([size, pageNumber = 0, allOrders]) => {
              return this.afs
                  .collection<IRule>(
                      'rules',
                      ref => {
                        const query: firestore.CollectionReference|
                            firestore.Query = ref;
                        const lastElement = pageNumber * 20;
                        const lastPageOrder = allOrders[lastElement];
                        return query.orderBy('id', 'asc')
                            .startAt(lastPageOrder.id)
                            .limit(20);
                      })
                  .stateChanges(['added', 'modified', 'removed'])
                  .map(actions => {
                    return actions.map(a => {
                      const data = a.payload.doc.data() as IRule;
                      const documentId = a.payload.doc.id;
                      return {documentId, ...data} as IRuleId;
                    });
                  });
            });
  }
  private filterBySize(size: number|null) {
    this.sizeFilter$.next(size);
  }
  private filterByPageNumber(pageNumber: number|null) {
    this.pageNumberFilter$.next(pageNumber);
  }
  all(): Observable<IRuleId[]> {
    return this.ruleIds$;
  }
  filterByPaging(page: MatPaginator): Promise<Page> {
    this.filterBySize(page.pageSize);
    this.filterByPageNumber(page.pageIndex);
    return this.getPagingOfOrders(page.pageSize, page.pageIndex);
  }
  getPagingOfOrders(size, pageNumber): Promise<Page> {
    return new Promise((resolve, reject) => {
      this.rulesCollection.valueChanges()
          .map(rules => {
            debugger;
            const totalPages = Math.round(rules.length / size);
            const totalElements = rules.length;
            const page = {
              size,
              pageNumber,
              totalElements,
              totalPages: (totalPages ? totalPages : 1)
            };
            return page;
          })
          .toPromise()
          .then(page => {
            debugger;
            resolve(page);
          })
          .catch(error => {
            console.log(error);
            reject(error);
          });
    });
  }
  add(params: IRule): Observable<IMessage> {
    const message = new StandartMessage(
        CommonConstants.SUCCESS, 'Registrado satisfactoriamente');
    const id = this.afs.createId();
    const rule: IRule = {
      id: id,
      commerceId: params.commerceId,
      ruleName: params.ruleName,
      ruleStatusId: params.ruleStatusId,
      description: params.description,
      rules: params.rules
    };
    return Observable.from(this.rulesCollection.add(rule))
        .map((x) => {
          return message;
        })
        .catch((error) => {
          message.code = CommonConstants.DANGER;
          message.message = error;
          return Observable.throw(message);
        });
  }
  update(params: IRuleId): Observable<IMessage> {
    const message = new StandartMessage(
        CommonConstants.SUCCESS, 'Actualizado satisfactoriamente');
    const id = this.afs.createId();
    const rule: IRule = {
      commerceId: params.commerceId,
      ruleName: params.ruleName,
      ruleStatusId: params.ruleStatusId,
      description: params.description,
      rules: params.rules
    };
    return Observable
        .from(this.afs.collection<IRule>(`rules`)
                  .doc(params.documentId)
                  .update(rule))
        .map((x) => {
          return message;
        })
        .catch((error) => {
          message.code = CommonConstants.DANGER;
          message.message = error;
          return Observable.throw(message);
        });
  }
  delete(params: IRuleId): Observable<IMessage> {
    const message = new StandartMessage(
        CommonConstants.SUCCESS, 'Eliminado satisfactoriamente');
    return Observable
        .from(
            this.afs.collection<IRule>(`rules`).doc(params.documentId).delete())
        .map((data) => {
          return message;
        })
        .catch((error) => {
          message.code = CommonConstants.DANGER;
          message.message = error;
          return Observable.throw(message);
        });
  }
  get(documentId: string): Observable<IRuleId> {
    return this.afs.doc<IRuleId>(`rules/${documentId}`).valueChanges();
  }
}
