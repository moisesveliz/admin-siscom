import {inject, TestBed} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {BusinessBaseService} from './business-base.service';

describe('BusinessBaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule(
        {providers: [BusinessBaseService], imports: [HttpModule]});
  });

  it('should be created',
     inject([BusinessBaseService], (service: BusinessBaseService) => {
       expect(service).toBeTruthy();
     }));
});
