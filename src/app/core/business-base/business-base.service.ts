import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';

import {CommonConstants} from '../../core/util/common-constants';

@Injectable()
export class BusinessBaseService {
  constructor(private http: HttpClient) {}

  getProducts(): Observable<any[]> {
    return this.http.get(CommonConstants.PRODUCTS_API)
        .map((resp: any) => resp.products)
        .catch((error: any) => Observable.throw(error.json()));
  }

  getParamsChildrenByMnemonicParentParam(params: {mnemonicParentParam: string}):
      Observable<any[]> {
    return this.http
        .get(CommonConstants.PARAMS_CHILDREN_BY_MNEMONIC_PARENT_PARAM)
        .map((resp: any) => {
          return resp.paramsChildrenByMnemonicParentParam.filter(
              x => x.hasOwnProperty(params.mnemonicParentParam));
        })
        .map((resp: any) => resp[0][params.mnemonicParentParam])
        .catch((error: any) => Observable.throw(error));
  }
}
