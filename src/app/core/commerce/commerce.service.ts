import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {CommonConstants} from '../util/common-constants';

@Injectable()
export class CommerceService {
  constructor(private http: HttpClient) {}

  getCommercesByAcquirer(params: any): Observable<any[]> {
    return this.http.get(CommonConstants.COMMERCES_BY_ACQUIRER_API)
        .map((resp: any) => resp.commercesByAcquirer)
        .catch((error: any) => Observable.throw(error));
  }
}
