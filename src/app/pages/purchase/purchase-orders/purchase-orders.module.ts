import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyModule } from '@ngx-formly/core';

import { PurchaseOrdersComponent } from './purchase-orders.component';
import { SharedModule } from '../../../shared/shared.module';

const routes: Routes = [{ path: '', data: { title: 'Ordenes de Compra' }, component: PurchaseOrdersComponent },
{ path: 'purchase-order', loadChildren: './purchase-order/purchase-order.module#PurchaseOrderModule' }];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PurchaseOrdersComponent]
})
export class PurchaseOrdersModule { }
