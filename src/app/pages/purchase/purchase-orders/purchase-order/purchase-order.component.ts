import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

import {DynamicControlService} from '../../../../core/dynamic-control/dynamic-control.service';
import {ControlBase} from '../../../../core/model/control-base';
import {PurchaseOrderService} from '../../../../core/purchase-order/purchase-order.service';

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.css']
})
export class PurchaseOrderComponent implements OnInit {
  controls$: Observable<ControlBase<any>[]>;
  form: FormGroup;
  payLoad = '';

  constructor(
      private dcs: DynamicControlService,
      private purchaseOrderService: PurchaseOrderService) {
    this.loadControls();
  }

  ngOnInit() {
    const controlSubscribe = this.controls$.subscribe(
        (controls) => {
          return this.form = this.dcs.toFormGroup(controls);
        },
        (errors) => {},
        () => {
          controlSubscribe.unsubscribe();
        });
  }

  save() {
    this.payLoad = JSON.stringify(this.form.value);
    console.log(this.payLoad);
  }

  private loadControls() {
    this.controls$ = this.purchaseOrderService.getControlsPurchaseOrder();
  }

  reset() {}
}
