import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {CommonConstants} from '../../../../core/util/common-constants';
import {SharedModule} from '../../../../shared/shared.module';

import {PurchaseOrderComponent} from './purchase-order.component';

const routes: Routes = [
  {
    path: 'register',
    data: {
      title: 'Registrar Orden Compra',
      action: CommonConstants.ACTION_REGISTER
    },
    component: PurchaseOrderComponent
  },
  {
    path: 'update/:documentId',
    data: {
      title: 'Actualizar Orden Compra',
      action: CommonConstants.ACTION_UPDATE
    },
    component: PurchaseOrderComponent
  },
  {
    path: 'update',
    data: {title: 'Actualizar Compra', action: CommonConstants.ACTION_UPDATE},
    component: PurchaseOrderComponent
  }
];

@NgModule({
  imports: [ReactiveFormsModule, RouterModule.forChild(routes), SharedModule],
  declarations: [PurchaseOrderComponent]
})
export class PurchaseOrderModule {}
