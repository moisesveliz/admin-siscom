import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgSelectModule} from '@ng-select/ng-select';

import {CommonConstants} from '../../../../core/util/common-constants';
import {NgbDateMomentParserFormatter} from '../../../../core/util/ngb-date-moment-parser-formatter';
import {SharedModule} from '../../../../shared/shared.module';

import {OrderComponent} from './order.component';

const routes: Routes = [
  {
    path: 'register',
    data: {title: 'Registrar Pedido', action: CommonConstants.ACTION_REGISTER},
    component: OrderComponent
  },
  {
    path: 'update/:documentId',
    data: {title: 'Actualizar Pedido', action: CommonConstants.ACTION_UPDATE},
    component: OrderComponent
  },
  {
    path: 'update',
    data: {title: 'Actualizar Pedido', action: CommonConstants.ACTION_UPDATE},
    component: OrderComponent
  }
];

@NgModule({
  imports: [
    ReactiveFormsModule, RouterModule.forChild(routes), SharedModule,
    NgSelectModule
  ],
  declarations: [OrderComponent]
})
export class OrderModule {}
