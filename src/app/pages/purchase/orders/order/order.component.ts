import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { ActivatedRoute } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';

import { BusinessBaseService } from '../../../../core/business-base/business-base.service';
import { CommonService } from '../../../../core/common/common.service';
import { IMessage } from '../../../../core/model/i-message';
import { IOrder } from '../../../../core/model/i-order';
import { IOrderId } from '../../../../core/model/i-order-id';
import { IRouteData } from '../../../../core/model/i-route-data';
import { StandartMessage } from '../../../../core/model/standart-message';
import { OrderService } from '../../../../core/order/order.service';
import { CommonConstants } from '../../../../core/util/common-constants';
import { ComponentsSupport } from '../../../../core/util/components-support';
import { ValidationSupport } from '../../../../core/util/validation-support';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  orderForm: FormGroup;
  products: Observable<any[]>;
  params: { action: string, documentId: string };
  orderId: IOrderId;
  variableFields: {
    nameButton: string,
    hiddenId: boolean,
    disabledId: boolean,
    searchClass: string,
    searchButtonName: string
  };
  city: any;
  cities: Array<any> = [
    { id: 'Amazonas', name: 'Amazonas' },
    { id: 'Áncash', name: 'Áncash' },
    { id: 'Apurímac', name: 'Apurímac' },
    { id: 'Arequipa', name: 'Arequipa' },
    { id: 'Ayacucho', name: 'Ayacucho' },
    { id: 'Cajamarca', name: 'Cajamarca' },
    { id: 'Callao', name: 'Callao' },
    { id: 'Cusco', name: 'Cusco' },
    { id: 'Huancavelica', name: 'Huancavelica' },
    { id: 'Huánuco', name: 'Huánuco' },
    { id: 'Ica', name: 'Ica' },
    { id: 'Junín', name: 'Junín' },
    { id: 'La Libertad', name: 'La Libertad' },
    { id: 'Lambayeque', name: 'Lambayeque' },
    { id: 'Lima', name: 'Lima' },
    { id: 'Loreto', name: 'Loreto' },
    { id: 'Madre de Dios', name: 'Madre de Dios' },
    { id: 'Moquegua', name: 'Moquegua' },
    { id: 'Pasco', name: 'Pasco' },
    { id: 'Piura', name: 'Piura' },
    { id: 'Puno', name: 'Puno' },
    { id: 'San Martín', name: 'San Martín' },
    { id: 'Tacna', name: 'Tacna' },
    { id: 'Tumbes', name: 'Tumbes' },
    { id: 'Ucayali', name: 'Ucayali' }
  ];

  constructor(
    private fb: FormBuilder, private businessBaseService: BusinessBaseService,
    private orderService: OrderService, private commonService: CommonService,
    private toastr: ToastrService, private route: ActivatedRoute) {
    this.initVariableFields();
    this.loadForm();
    this.loadParams();
    this.isRegistrationOrUpdate();
  }
  initVariableFields() {
    this.variableFields = {
      nameButton: 'Registrar',
      hiddenId: true,
      disabledId: true,
      searchClass: 'btn btn-info',
      searchButtonName: 'Buscar'
    };
  }
  loadParams(): void {
    const action = this.route.snapshot.data.action;
    let documentId = null;
    this.route.params.subscribe(params => {
      documentId = params.documentId;
    });
    this.params = { action: action, documentId: documentId };
  }
  loadOrder(documentId: string) {
    this.orderService.get(documentId).subscribe((orderId: IOrderId) => {
      if (!orderId) {
        this.orderId = {
          'amount': 0,
          'date': '',
          'product': -1,
          'sustention': '',
          'documentId': ''
        };
      } else {
        this.orderId = {
          amount: orderId.amount,
          date: orderId.date,
          product: orderId.product,
          sustention: orderId.sustention,
          documentId: documentId
        };
      }

      this.orderId.date = ComponentsSupport.convertStringDateToNgbDateStruct(
        this.orderId.date, CommonConstants.FORMAT_LARGE_DDMMYYYY);
      this.loadOrderInForm();
    });
  }
  searchOrder(): void {
    const documentId = this.orderForm.value.documentId;
    this.loadOrder(documentId);
  }
  isRegistrationOrUpdate() {
    if (this.params.action === CommonConstants.ACTION_UPDATE) {
      this.variableFields.nameButton = 'Actualizar';
      // this.orderForm.controls.documentId.disable();
      if (this.params.documentId) {
        this.loadOrder(this.params.documentId);
      } else {
        this.variableFields.hiddenId = false;
        this.variableFields.disabledId = false;
        // this.orderForm.controls.documentId.enable();
      }
    }
  }
  loadOrderInForm(): void {
    this.orderForm.setValue(this.orderId, { onlySelf: true });
  }
  loadForm(): void {
    this.orderForm = this.fb.group({
      documentId: [''],
      date: [
        ComponentsSupport.getTodayNgbDateStruct(),
        [ValidationSupport.notEmptyDateValidator]
      ],
      amount: ['', [ValidationSupport.amountValidator]],
      sustention: ['', [ValidationSupport.notEmptyStringValidator]],
      product: [
        CommonConstants.EMPTY_VALUE, [ValidationSupport.selectedItemValidator]
      ],
      city: [[], ValidationSupport.notItemInArrayValidator]
    });
  }
  ngOnInit() {
    this.getProducts();
  }
  getProducts(): void {
    this.products = this.businessBaseService.getProducts();
  }
  save(): void {
    if (this.orderForm.valid) {
      const order: IOrderId = {
        documentId: this.orderForm.value.documentId,
        date:
        (this.orderForm.value.date.day + '-' +
          this.orderForm.value.date.month + '-' +
          this.orderForm.value.date.year),
        amount: this.orderForm.value.amount,
        sustention: this.orderForm.value.sustention,
        product: this.orderForm.value.product
      };
      if (this.params.action === CommonConstants.ACTION_UPDATE) {
        this.update(order);
      } else {
        this.add(order);
      }
    }
  }
  private update(orderId: IOrderId) {
    this.orderService.update(orderId).subscribe(
      (messageModel: StandartMessage) => {
        this.commonService.showMessage(messageModel);
      });
  }
  private add(order: IOrder) {
    this.orderService.add(order).subscribe((messageModel: StandartMessage) => {
      this.reset();
      this.commonService.showMessage(messageModel);
    });
  }
  reset(): void {
    this.orderForm.reset();
  }
}
