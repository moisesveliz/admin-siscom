import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {ActivatedRoute, Router, RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {ToastrModule, ToastrService} from 'ngx-toastr';

import {environment} from '../../../../../environments/environment';
import {BusinessBaseService} from '../../../../core/business-base/business-base.service';
import {CommonService} from '../../../../core/common/common.service';
import {OrderService} from '../../../../core/order/order.service';
import {ControlMessagesComponent} from '../../../../shared/control-messages/control-messages.component';
import {SharedModule} from '../../../../shared/shared.module';
import {CommonConstants} from '../../../../shared/util/common.constants';

import {OrderComponent} from './order.component';

const routes: Routes = [
  {
    path: 'register',
    data: {title: 'Registrar Pedido', action: CommonConstants.ACTION_REGISTER},
    component: OrderComponent
  },
  {
    path: 'update/:documentId',
    data:
        {title: 'Actualizar Pedido222', action: CommonConstants.ACTION_UPDATE},
    component: OrderComponent
  },
  {
    path: 'update',
    data:
        {title: 'Actualizar Pedido333', action: CommonConstants.ACTION_UPDATE},
    component: OrderComponent
  }
];

// ADDED CLASS
class MockRouter {
  navigateByUrl(url: string) {
    return url;
  }
}

describe('OrderComponent', () => {
  let component: OrderComponent;
  let fixture: ComponentFixture<OrderComponent>;
  let mockRouter: any;
  let mockActivatedRoute: any;

  beforeEach(async(() => {
    TestBed
        .configureTestingModule({
          declarations: [OrderComponent, ControlMessagesComponent],
          imports: [
            HttpModule, ReactiveFormsModule, NgbModule.forRoot(),
            AngularFireModule.initializeApp(environment.firebase),
            AngularFirestoreModule, AngularFireAuthModule, SharedModule,
            ToastrModule.forRoot(), RouterModule.forChild(routes)
          ],
          providers: [
            BusinessBaseService, OrderService, CommonService, ToastrService,
            {provide: Router, useClass: MockRouter}
          ]
        })
        .compileComponents();
  }));

  /*beforeEach(() => {
    fixture = TestBed.createComponent(OrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });*/

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/

  it('primer test', () => {
    expect(true).toBe(true);
  });
});
