import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {SharedModule} from '../../../shared/shared.module';

import {OrdersComponent} from './orders.component';

const routes: Routes = [
  {path: '', data: {title: 'Pedidos'}, component: OrdersComponent},
  {path: 'order', loadChildren: './order/order.module#OrderModule'}
];

@NgModule({
  imports: [
    NgxDatatableModule, ReactiveFormsModule, SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrdersComponent]
})
export class OrdersModule {}
