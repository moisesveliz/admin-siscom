import {ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Rx';

import {CommonService} from '../../../core/common/common.service';
import {IOrder} from '../../../core/model/i-order';
import {IOrderId} from '../../../core/model/i-order-id';
import {Page} from '../../../core/model/page';
import {StandartMessage} from '../../../core/model/standart-message';
import {OrderService} from '../../../core/order/order.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  page: Page = new Page();
  rows$: Observable<IOrderId[]>;
  page$: Observable<Page>;
  columns = [];

  constructor(
      private orderService: OrderService, private commonService: CommonService,
      private router: Router) {
    this.page.pageNumber = 0;
    this.page.size = 20;
  }

  ngOnInit() {
    this.loadOrders();
    this.setPage({offset: 0});
  }
  loadOrders(): void {
    this.rows$ = this.orderService.all();
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.orderService.filterByPaging(this.page).then(page => {
      this.page = page;
    });
  }
  deleteOrder(order: IOrderId) {
    this.orderService.delete(order).subscribe(
        (messageModel: StandartMessage) => {
          this.commonService.showMessage(messageModel);
        });
  }
  editOrder(order: IOrderId) {
    console.log(JSON.stringify(order));
    this.router.navigate(['/orders/order/update', {order: order}]);
  }
}
