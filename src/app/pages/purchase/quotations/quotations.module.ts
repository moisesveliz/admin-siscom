import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { QuotationsComponent } from './quotations.component';

const routes: Routes = [{
  path: '',
  data: {
      title: 'Cotizaciones'
    },
  component: QuotationsComponent
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [QuotationsComponent]
})
export class QuotationsModule { }
