import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {SharedModule} from '../../shared/shared.module';

import {RuleComponent} from './rule/rule.component';
import {RulesComponent} from './rules.component';

const routes: Routes = [
  {path: '', data: {title: 'Reglas'}, component: RulesComponent},
  {path: 'rule', loadChildren: './rule/rule.module#RuleModule'}
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [RulesComponent]
})
export class RulesModule {}
