import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, PageEvent} from '@angular/material';
import {Observable} from 'rxjs/Observable';

import {IRule} from '../../core/model/i-rule';
import {IRuleId} from '../../core/model/i-rule-id';
import {Page} from '../../core/model/page';
import {RuleService} from '../../core/rule/rule.service';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss']
})
export class RulesComponent implements OnInit {
  displayedColumns: Array<any>;
  dataSource: MatTableDataSource<IRule>;
  serverColumns: Array<any> = [
    {label: 'ID', value: 'id'}, {label: 'Comercio', value: 'commerceId'},
    {label: 'Regla', value: 'ruleName'},
    {label: 'Estado', value: 'ruleStatusId'},
    {label: 'Descripción', value: 'description'}
  ];
  // MdPaginator Inputs
  pageSize = 20;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 20, 100];
  length = 2;
  // MdPaginator Output
  pageEvent: PageEvent;

  rows$: Observable<IRule[]>;
  page$: Observable<Page>;
  columns: Array<any> = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private ruleService: RuleService) {}
  ngOnInit() {
    this.prepareColumns();
    this.prepareDisplayedColumns();
    this.loadRules();
    this.setPage({offset: 0});
  }
  private loadRules(): void {
    this.ruleService.all().subscribe(
        dataSource => this.setDataSource(dataSource), (error) => {}, () => {
          this.setPaginator();
          this.setSort();
        });
  }
  private prepareColumns(): void {
    this.columns = this.serverColumns.map(serverColumn => {
      const column = {
        columnDef: serverColumn.value,
        header: serverColumn.label,
        cell: (row: IRule) => `${row[serverColumn.value]}`
      };
      return column;
    });
  }
  private prepareDisplayedColumns(): void {
    this.displayedColumns = this.serverColumns.map(x => x.value);
  }
  private setDataSource(dataSource: any): void {
    this.dataSource = new MatTableDataSource(dataSource);
  }
  private setPaginator(): void {
    this.paginator.pageSizeOptions = [5, 10, 25, 100];
    this.dataSource.paginator = this.paginator;
  }
  private setSort(): void {
    this.dataSource.sort = this.sort;
  }
  setPage(pageInfo) {
    this.ruleService.filterByPaging(this.paginator)
        .then(page => {
          debugger;
          this.length = page.totalElements;
          this.pageIndex = page.pageNumber;
        })
        .catch(error => console.log(error));
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();  // Remove whitespace
    filterValue =
        filterValue.toLowerCase();  // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}
