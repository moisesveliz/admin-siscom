import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ErrorStateMatcher, MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SortablejsOptions} from 'angular-sortablejs/dist';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

import {BusinessBaseService} from '../../../../core/business-base/business-base.service';
import {CommerceService} from '../../../../core/commerce/commerce.service';
import {RuleService} from '../../../../core/rule/rule.service';
import {CommonConstants} from '../../../../core/util/common-constants';
import {ComponentsSupport} from '../../../../core/util/components-support';
import {FunctionalSupport} from '../../../../core/util/functional-support';
import {ValidationSupport} from '../../../../core/util/validation-support';

@Component({
  selector: 'app-add-rule-dialog',
  templateUrl: './add-rule-dialog.component.html',
  styleUrls: ['./add-rule-dialog.component.scss']
})
export class AddRuleDialogComponent extends FunctionalSupport implements
    OnInit {
  addRuleForm: FormGroup;
  variableFieldsByAcquirer: any[];
  functionFieldsByAcquirer: any[];
  varFuncFieldsByAcquirer: any[] = [];
  valueTagAndValidatorByFieldAndAcquirer: any;
  fieldTypeByAcquirer$: Observable<any[]>;
  conditionsByFieldByAcquirer: any[];
  fieldDescription: string;
  constructor(
      private fb: FormBuilder, private ruleService: RuleService,
      private commerceService: CommerceService,
      private businessBaseService: BusinessBaseService,
      @Inject(MAT_DIALOG_DATA) private ruleData: any,
      private ruleDialogRef: MatDialogRef<AddRuleDialogComponent>) {
    super();
    this.loadAddRuleForm();
  }
  isValid(control): Boolean {
    console.log(control);
    return control.touched;
  }
  ngOnInit() {
    this.loadFieldTypeByAcquirer();
  }
  addRule(): void {
    if (this.addRuleForm.valid) {
      const rule: any = {
        field: this.addRuleForm.value.field,
        condition: this.addRuleForm.value.condition,
        value: this.addRuleForm.value.value,
        secondaryValue: this.addRuleForm.value.secondaryValue,
        label: `${this.addRuleForm.value.field} ${
            this.addRuleForm.value.condition} ${this.addRuleForm.value.value}`
      };
      // this.ruleData.rules.value.push(rule);
      const rules: Array<any> = this.ruleData.rules.value;
      rules.push(rule);
      this.ruleData.rules.setValue(rules);
      this.ruleDialogRef.close();
    } else {
      this.validateAllFields();
    }
  }
  validateAllFields(): void {
    Object.keys(this.addRuleForm.controls).forEach(field => {
      const control = this.addRuleForm.get(field);
      control.markAsTouched({onlySelf: true});
    });
  }
  onChangeFieldType($event?) {
    this.resetFieldTypeDependencies();
    this.refreshVarFuncFieldsByAcquirer();
  }
  onChangeField($event?) {
    this.resetValueAndSecondaryValue();
    const fieldValue = this.addRuleForm.controls.field.value;
    if (fieldValue) {
      this.evaluateFieldDescription(fieldValue);
      this.loadConditionsByFieldByAcquirer(fieldValue);
      this.loadValueTagAndValidatorByFieldAndAcquirer(fieldValue);
    } else {
      this.cleanConditionsByFieldByAcquirer();
    }
  }
  private cleanConditionsByFieldByAcquirer(): void {
    this.conditionsByFieldByAcquirer = [];
  }
  private loadFieldTypeByAcquirer(): void {
    this.fieldTypeByAcquirer$ = this.ruleService.getFieldTypesByAcquirer({});
    const unsubscribeFieldTypeByAcquirer = this.fieldTypeByAcquirer$.subscribe(
        fieldTypeByAcquirer => {
          this.refreshVarFuncFieldsByAcquirer();
        },
        error => {},
        () => {
          unsubscribeFieldTypeByAcquirer.unsubscribe();
        });
  }
  private loadConditionsByFieldByAcquirer(field: any): void {
    const unsubscribeConditionsByFieldByAcquirer =
        this.ruleService.getConditionsByFieldByAcquirer({field: field})
            .subscribe(
                conditionsByFieldByAcquirer => {
                  this.conditionsByFieldByAcquirer =
                      conditionsByFieldByAcquirer;
                },
                error => {},
                () => unsubscribeConditionsByFieldByAcquirer.unsubscribe());
  }
  private loadFunctionFieldsByAcquirer(): void {
    if (!this.functionFieldsByAcquirer) {
      const unsubscribeFunctionFieldsByAcquirer =
          this.ruleService.getFunctionFieldsByAcquirer({}).subscribe(
              functionFieldsByAcquirer => {
                this.functionFieldsByAcquirer = functionFieldsByAcquirer;
                this.loadArrVarFuncFieldsByAcquirer(functionFieldsByAcquirer);
              },
              (error) => {},
              () => {
                unsubscribeFunctionFieldsByAcquirer.unsubscribe();
              });
    } else {
      this.loadArrVarFuncFieldsByAcquirer(this.functionFieldsByAcquirer);
    }
  }
  private loadVariableFieldsByAcquirer(): void {
    if (!this.variableFieldsByAcquirer) {
      const unsubscribeVariableFieldsByAcquirer =
          this.ruleService.getVariableFieldsByAcquirer({}).subscribe(
              variableFieldsByAcquirer => {
                this.variableFieldsByAcquirer = variableFieldsByAcquirer;
                this.loadArrVarFuncFieldsByAcquirer(variableFieldsByAcquirer);
              },
              (error) => {},
              () => {
                unsubscribeVariableFieldsByAcquirer.unsubscribe();
              });
    } else {
      this.loadArrVarFuncFieldsByAcquirer(this.variableFieldsByAcquirer);
    }
  }
  private loadArrVarFuncFieldsByAcquirer(varFuncFieldsByAcquirer): void {
    this.varFuncFieldsByAcquirer = [];
    const arrVarFuncFieldsByAcquirerFiltered =
        this.validateAndDiscardRuleFields(varFuncFieldsByAcquirer);
    this.varFuncFieldsByAcquirer.push(...arrVarFuncFieldsByAcquirerFiltered);
  }
  private validateAndDiscardRuleFields(varFuncFieldsByAcquirer: any[]): any[] {
    return this.ruleService.validateAndDiscardRuleFields(
        varFuncFieldsByAcquirer, this.ruleData.rules.value);
  }
  private loadValueTagAndValidatorByFieldAndAcquirer(field) {
    const unsubscribeValueTagAndValidatorByFieldAndAcquirer =
        this.ruleService
            .getValueTagAndValidatorByFieldAndAcquirer({field: field})
            .subscribe(
                valueTagAndValidatorByFieldAndAcquirer => {
                  this.transformAndPrepareValidatorsValueAndSecondaryValue(
                      valueTagAndValidatorByFieldAndAcquirer);
                },
                error => {},
                () => unsubscribeValueTagAndValidatorByFieldAndAcquirer
                          .unsubscribe());
  }
  private transformAndPrepareValidatorsValueAndSecondaryValue(
      valueTagAndValidatorByFieldAndAcquirer): void {
    this.valueTagAndValidatorByFieldAndAcquirer =
        valueTagAndValidatorByFieldAndAcquirer;
    this.transformValidatorsValue();
    this.transformValidatorsSecondaryValue();
    this.setValidatorsValue();
  }
  private transformValidatorsValue() {
    this.valueTagAndValidatorByFieldAndAcquirer.validators =
        ComponentsSupport.getValidatorsFromArrStrValidators(
            this.valueTagAndValidatorByFieldAndAcquirer.validators);
  }
  private setValidatorsValue() {
    this.addRuleForm.controls.value.setValidators(
        this.valueTagAndValidatorByFieldAndAcquirer.validators);
    this.updateValueOnlyControlTypeIsRadio();
  }
  private updateValueOnlyControlTypeIsRadio() {
    if (this.valueTagAndValidatorByFieldAndAcquirer.controlType === 'radio') {
      const valueFiltered =
          this.valueTagAndValidatorByFieldAndAcquirer.options.filter(value => {
            return value.selected;
          });
      const [valueFilteredDestructuring] = valueFiltered;
      this.addRuleForm.controls.value.setValue(
          valueFilteredDestructuring.value);
    }
  }
  private transformValidatorsSecondaryValue() {
    if (this.valueTagAndValidatorByFieldAndAcquirer.secondaryTag) {
      this.valueTagAndValidatorByFieldAndAcquirer.secondaryValidators =
          ComponentsSupport.getValidatorsFromArrStrValidators(
              this.valueTagAndValidatorByFieldAndAcquirer.secondaryValidators);
      this.setValidatorsSecondaryValue();
    }
  }
  private setValidatorsSecondaryValue() {
    this.addRuleForm.controls.secondaryValue.setValidators(
        this.valueTagAndValidatorByFieldAndAcquirer.secondaryValidators);
  }
  private loadAddRuleForm(): void {
    this.addRuleForm = this.fb.group({
      fieldType: [
        CommonConstants.FIELD_TYPE_VARIABLE,
        [ValidationSupport.selectedItemValidator]
      ],
      field: [
        CommonConstants.EMPTY_VALUE, [ValidationSupport.selectedItemValidator]
      ],
      condition: [
        CommonConstants.EMPTY_VALUE, [ValidationSupport.selectedItemValidator]
      ],
      value: [CommonConstants.EMPTY_VALUE],
      secondaryValue: [CommonConstants.EMPTY_VALUE]
    });
  }
  private resetAddRuleForm() {
    this.addRuleForm.reset(
        {
          fieldType: CommonConstants.FIELD_TYPE_VARIABLE,
          field: CommonConstants.EMPTY_VALUE,
          condition: CommonConstants.EMPTY_VALUE,
          value: CommonConstants.EMPTY_VALUE,
          secondaryValue: CommonConstants.EMPTY_VALUE
        },
        {onlySelf: true});
    this.setToNullFieldDescription();
    this.setToNullValueAndSecondaryValue();
  }
  private resetFieldTypeDependencies(): void {
    this.addRuleForm.controls.field.reset(CommonConstants.EMPTY_VALUE);
    this.addRuleForm.controls.condition.reset(CommonConstants.EMPTY_VALUE);
    this.addRuleForm.controls.value.reset(CommonConstants.EMPTY_VALUE);
    this.addRuleForm.controls.secondaryValue.reset(CommonConstants.EMPTY_VALUE);
    this.setToNullFieldDescription();
    this.setToNullValueAndSecondaryValue();
  }
  private refreshVarFuncFieldsByAcquirer() {
    const fieldType = this.addRuleForm.controls.fieldType.value;
    if (CommonConstants.FIELD_TYPE_FUNCTION === fieldType) {
      this.loadFunctionFieldsByAcquirer();
    } else if (CommonConstants.FIELD_TYPE_VARIABLE === fieldType) {
      this.loadVariableFieldsByAcquirer();
    }
  }
  private setToNullFieldDescription(): void {
    this.fieldDescription = null;
  }
  private setToNullValueAndSecondaryValue(): void {
    this.valueTagAndValidatorByFieldAndAcquirer = null;
  }
  private resetValueAndSecondaryValue(): void {
    this.addRuleForm.controls.value.reset(CommonConstants.EMPTY_VALUE);
    this.addRuleForm.controls.secondaryValue.reset(CommonConstants.EMPTY_VALUE);
    this.setToNullValueAndSecondaryValue();
  }
  private evaluateFieldDescription(fieldValue): void {
    const arrVarFuncFieldsByAcquirerFiltered =
        this.varFuncFieldsByAcquirer.filter(
            field => fieldValue === field.value);
    const [fieldSelected] = arrVarFuncFieldsByAcquirerFiltered;
    this.fieldDescription = fieldSelected ? fieldSelected.description : null;
  }
}
