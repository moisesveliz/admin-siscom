import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SortablejsModule} from 'angular-sortablejs';

import {CommonConstants} from '../../../core/util/common-constants';
import {MaterialModule} from '../../../shared/material/material.module';
import {SharedModule} from '../../../shared/shared.module';

import {AddRuleDialogComponent} from './add-rule-dialog/add-rule-dialog.component';
import {RuleComponent} from './rule.component';

const routes: Routes = [
  {
    path: 'register',
    data: {title: 'Registrar Regla', action: CommonConstants.ACTION_REGISTER},
    component: RuleComponent
  },
  {
    path: 'update/:documentId',
    data: {title: 'Actualizar Regla', action: CommonConstants.ACTION_UPDATE},
    component: RuleComponent
  },
  {
    path: 'update',
    data: {title: 'Actualizar Regla', action: CommonConstants.ACTION_UPDATE},
    component: RuleComponent
  }
];

@NgModule({
  imports: [
    SharedModule, RouterModule.forChild(routes),
    SortablejsModule.forRoot({animation: 150})
  ],
  declarations: [RuleComponent, AddRuleDialogComponent],
  entryComponents: [AddRuleDialogComponent]
})
export class RuleModule {}
