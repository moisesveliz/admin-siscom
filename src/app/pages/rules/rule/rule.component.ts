import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ErrorStateMatcher, MatDialog} from '@angular/material';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SortablejsOptions} from 'angular-sortablejs/dist';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

import {BusinessBaseService} from '../../../core/business-base/business-base.service';
import {CommerceService} from '../../../core/commerce/commerce.service';
import {CommonService} from '../../../core/common/common.service';
import {IRule} from '../../../core/model/i-rule';
import {IRuleId} from '../../../core/model/i-rule-id';
import {StandartMessage} from '../../../core/model/standart-message';
import {RuleService} from '../../../core/rule/rule.service';
import {CommonConstants} from '../../../core/util/common-constants';
import {ComponentsSupport} from '../../../core/util/components-support';
import {FunctionalSupport} from '../../../core/util/functional-support';
import {ValidationSupport} from '../../../core/util/validation-support';

import {AddRuleDialogComponent} from './add-rule-dialog/add-rule-dialog.component';

@Component({
  selector: 'app-rule',
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.scss']
})
export class RuleComponent extends FunctionalSupport implements OnInit {
  generalRuleForm: FormGroup;
  trash: any[] = [];
  rules = new FormControl([], [ValidationSupport.notItemInArrayValidator]);
  commerces$: Observable<any[]>;
  ruleStatus$: Observable<any[]>;
  hide: Boolean = true;

  ngOnInit() {
    this.loadCommercesByAcquirer();
    this.loadRuleStatus();
  }
  constructor(
      private fb: FormBuilder, private dialog: MatDialog,
      private ruleService: RuleService,
      private commerceService: CommerceService,
      private businessBaseService: BusinessBaseService,
      private commonService: CommonService) {
    super();
    this.loadGeneralRuleForm();
  }
  openRulesModal() {
    this.dialog.open(
        AddRuleDialogComponent, {width: '70%', data: {rules: this.rules}});
  }
  saveRule() {
    if (this.generalRuleForm.valid) {
      const rule: IRuleId = {
        commerceId: this.generalRuleForm.controls.commerceId.value,
        ruleName: this.generalRuleForm.controls.ruleName.value,
        ruleStatusId: this.generalRuleForm.controls.ruleStatusId.value,
        description: this.generalRuleForm.controls.description.value,
        rules: this.rules.value
      };
      this.add(rule);
    } else {
      Object.keys(this.generalRuleForm.controls).forEach(field => {
        const control = this.generalRuleForm.get(field);
        control.markAsTouched({onlySelf: true});
      });
      this.rules.markAsTouched({onlySelf: true});
    }
  }
  private add(rule: IRule) {
    this.ruleService.add(rule).subscribe((messageModel: StandartMessage) => {
      this.resetRuleForm();
      this.commonService.showMessage(messageModel);
    });
  }
  resetRuleForm(): void {}
  removeRule(rule: any): void {
    const index = this.rules.value.indexOf(rule);
    if (index >= 0) {
      this.rules.value.splice(index, 1);
    }
    const rules = this.rules.value;
    this.rules.setValue(rules);
  }
  private loadCommercesByAcquirer(): void {
    this.commerces$ = this.commerceService.getCommercesByAcquirer({});
    const unsubscribeCommerces =
        this.commerces$.subscribe(commerces => {}, error => {}, () => {
          unsubscribeCommerces.unsubscribe();
        });
  }
  private loadRuleStatus(): void {
    this.ruleStatus$ =
        this.businessBaseService.getParamsChildrenByMnemonicParentParam({
          mnemonicParentParam: CommonConstants.MNEMONIC_PARENT_PARAM_RULE_STATUS
        });
    const unsubscribeRuleStatus =
        this.ruleStatus$.subscribe(ruleStatus => {}, error => {}, () => {
          unsubscribeRuleStatus.unsubscribe();
        });
  }
  private loadGeneralRuleForm(): void {
    this.generalRuleForm = this.fb.group({
      commerceId: [
        CommonConstants.EMPTY_VALUE, [ValidationSupport.selectedItemValidator]
      ],
      ruleName: [
        CommonConstants.EMPTY_VALUE, [ValidationSupport.notEmptyStringValidator]
      ],
      ruleStatusId: [
        CommonConstants.EMPTY_VALUE, [ValidationSupport.selectedItemValidator]
      ],
      description: [
        CommonConstants.EMPTY_VALUE, [ValidationSupport.notEmptyStringValidator]
      ]
    });
  }
}
