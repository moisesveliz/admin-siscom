import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BreadcrumbComponent } from '../shared/breadcrumb/breadcrumb.component';
import { NavigationComponent } from '../shared/header-navigation/navigation.component';
import { NAV_DROPDOWN_DIRECTIVES } from '../shared/nav-dropdown.directive';
import { RightSidebarComponent } from '../shared/right-sidebar/rightsidebar.component';
import { SIDEBAR_TOGGLE_DIRECTIVES } from '../shared/sidebar.directive';
import { SidebarComponent } from '../shared/sidebar/sidebar.component';

import { PagesRoutingModule } from './pages-routing.module';
import { PageComponent } from './pages.component';

@NgModule({
  imports: [CommonModule, PagesRoutingModule],
  declarations: [
    PageComponent, NavigationComponent, BreadcrumbComponent, SidebarComponent,
    RightSidebarComponent, SIDEBAR_TOGGLE_DIRECTIVES, NAV_DROPDOWN_DIRECTIVES
  ]
})
export class PagesModule { }
