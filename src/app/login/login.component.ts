import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {
  constructor(public router: Router) {}

  ngOnInit() {}

  ngAfterViewInit() {
    $(function() {
      $('.preloader').fadeOut();
    });
    $(function() {
      (<any>$('[data-toggle="tooltip"]')).tooltip();
    });
    $('#to-recover').on('click', function() {
      $('#loginform').slideUp();
      $('#recoverform').fadeIn();
    });
  }

  onLoggedin() {
    localStorage.setItem('isLoggedin', 'true');
  }

  onToRecoverClick() {}
}
