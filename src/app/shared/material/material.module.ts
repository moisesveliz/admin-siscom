import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSortModule, MatTableModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatDialogModule} from '@angular/material/dialog';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatToolbarModule} from '@angular/material/toolbar';

@NgModule({
  imports: [
    CommonModule, MatFormFieldModule, MatInputModule, MatCardModule,
    MatGridListModule, MatSelectModule, MatButtonModule, MatChipsModule,
    MatIconModule, MatDialogModule, MatRadioModule, MatTableModule,
    MatSortModule, MatPaginatorModule, MatListModule, MatToolbarModule
  ],
  exports: [
    MatFormFieldModule, MatInputModule, MatCardModule, MatGridListModule,
    MatSelectModule, MatButtonModule, MatChipsModule, MatIconModule,
    MatDialogModule, MatRadioModule, MatTableModule, MatPaginatorModule,
    MatSortModule, MatListModule, MatToolbarModule
  ]
})
export class MaterialModule {}
