import {AfterViewInit, Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {OptionGroup} from '../../core/model/option-group';
import {SecurityService} from '../../core/security/security.service';

@Component({selector: 'app-sidebar', templateUrl: './sidebar.component.html'})
export class SidebarComponent implements AfterViewInit {
  menu: Observable<OptionGroup[]>;

  constructor(private securityService: SecurityService) {
    this.loadMenu();
  }

  ngAfterViewInit() {
    $(function() {
      const url = window.location.toString();
      let element = $('ul#sidebarnav a')
                        .filter(function() {
                          const a = <HTMLAnchorElement>this;
                          return (a.href === url ? true : false);
                        })
                        .addClass('active')
                        .parent()
                        .addClass('active');
      while (true) {
        if (element.is('li')) {
          element = element.parent().addClass('in').parent().addClass('active');
        } else {
          break;
        }
      }

      (<any>$('#sidebarnav')).metisMenu();
    });
  }

  loadMenu() {
    this.menu = this.securityService.getMenu();
  }
}
