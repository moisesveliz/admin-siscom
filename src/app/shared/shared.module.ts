import {CommonModule} from '@angular/common';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {NgbDateMomentParserFormatter} from '../core/util/ngb-date-moment-parser-formatter';

import {ControlMessagesComponent} from './control-messages/control-messages.component';
import {ControlMessagesDirective} from './control-messages/control-messages.directive';
import {DynamicFormControlComponent} from './dynamic-form-control/dynamic-form-control.component';
import {DynamicMatErrorDirective} from './dynamic-mat-error/dynamic-mat-error.directive';
import {DynamicMatError2Directive} from './dynamic-mat-error/dynamic-mat-error2.directive';
import {MaterialModule} from './material/material.module';

@NgModule({
  imports: [
    ReactiveFormsModule, CommonModule, MaterialModule, TranslateModule,
    NgbModule.forRoot()
  ],
  declarations: [
    ControlMessagesComponent, DynamicFormControlComponent,
    ControlMessagesDirective, DynamicMatErrorDirective,
    DynamicMatError2Directive
  ],
  providers: [{
    provide: NgbDateParserFormatter,
    useFactory: () => {
      return new NgbDateMomentParserFormatter('DD-MM-YYYY');
    }
  }],
  exports: [
    ReactiveFormsModule, MaterialModule, ControlMessagesComponent, CommonModule,
    TranslateModule, DynamicFormControlComponent, NgbModule,
    ControlMessagesDirective, DynamicMatErrorDirective,
    DynamicMatError2Directive
  ]
})
export class SharedModule {}
