import {Directive, ElementRef, Input, Renderer, TemplateRef, ViewContainerRef} from '@angular/core';
import {OnInit} from '@angular/core/src/metadata/lifecycle_hooks';
import {FormControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {LangChangeEvent} from '@ngx-translate/core';

import {ILang} from '../../core/model/i-lang';
import {ValidationSupport} from '../../core/util/validation-support';

@Directive({selector: '[appDynamicMatError]'})
export class DynamicMatErrorDirective implements OnInit {
  private control: FormControl;
  @Input()
  set appDynamicMatError(control: FormControl) {
    this.control = control;
    this.searchErrorMessage();
  }
  constructor(
      private el: ElementRef, private translate: TranslateService,
      private renderer: Renderer) {}
  ngOnInit(): void {
    this.changeLangErrorMessage();
  }
  changeLangErrorMessage(): void {
    this.translate.onLangChange.subscribe(
        (event: LangChangeEvent) => this.searchErrorMessage());
  }
  searchErrorMessage(): void {
    console.log('searchErrorMessage');
    console.log(this.control);
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) &&
          this.control.touched) {
        this.defineErrorMessage(propertyName);
      }
    }
  }
  defineErrorMessage(propertyName: string): void {
    if (this.control.errors.hasOwnProperty(propertyName) &&
        this.control.touched) {
      this.translateErrorMessage(propertyName);
    }
  }
  translateErrorMessage(propertyName: string): void {
    const lang: ILang = ValidationSupport.getValidatorErrorMessage(
        propertyName, this.control.errors[propertyName]);
    this.translate.get(lang.name, lang.values)
        .subscribe(errorMessage => this.rendererMatErrorElement(errorMessage))
        .unsubscribe();
  }
  rendererMatErrorElement(errorMessage: string): void {
    this.renderer.setElementProperty(
        this.el.nativeElement, 'textContent', errorMessage);
  }
}
