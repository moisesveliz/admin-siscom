import {Directive, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {FormControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {LangChangeEvent} from '@ngx-translate/core';

import {ILang} from '../../core/model/i-lang';
import {ValidationSupport} from '../../core/util/validation-support';

@Directive({selector: '[appDynamicMatError2]'})
export class DynamicMatError2Directive implements OnInit {
  private control: FormControl;
  constructor(
      private templateRef: TemplateRef<any>,
      private viewContainer: ViewContainerRef,
      private translate: TranslateService) {}
  @Input()
  set appDynamicMatError2(control: FormControl) {
    this.control = control;
    this.viewContainer.clear();
    this.searchErrorMessage();
  }
  ngOnInit(): void {
    this.changeLangErrorMessage();
  }
  changeLangErrorMessage(): void {
    this.translate.onLangChange.subscribe(
        (event: LangChangeEvent) => this.searchErrorMessage());
  }
  searchErrorMessage(): void {
    console.log('searchErrorMessage2');
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) &&
          this.control.touched) {
        this.defineErrorMessage(propertyName);
      }
    }
  }
  defineErrorMessage(propertyName: string): void {
    if (this.control.errors.hasOwnProperty(propertyName) &&
        this.control.touched) {
      this.translateErrorMessage(propertyName);
    }
  }
  translateErrorMessage(propertyName: string): void {
    const lang: ILang = ValidationSupport.getValidatorErrorMessage(
        propertyName, this.control.errors[propertyName]);
    this.translate.get(lang.name, lang.values)
        .subscribe(errorMessage => this.rendererMatErrorElement(errorMessage))
        .unsubscribe();
  }
  rendererMatErrorElement(errorMessage: string): void {
    this.viewContainer.createEmbeddedView(
        this.templateRef, {$implicit: errorMessage});
  }
}
