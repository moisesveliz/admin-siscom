import {Directive, ElementRef, HostBinding, Input, OnInit, Renderer} from '@angular/core';
import {OnChanges} from '@angular/core/src/metadata/lifecycle_hooks';
import {FormControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

import {ILang} from '../../core/model/i-lang';
import {ValidationSupport} from '../../core/util/validation-support';

@Directive({selector: '[appControlMessages]'})
export class ControlMessagesDirective implements OnInit, OnChanges {
  @HostBinding('display')
  ngif: boolean;
  errorMessage: string;
  @Input() control: FormControl;
  constructor(
      private el: ElementRef, private translate: TranslateService,
      private renderer: Renderer) {}

  searchErrorMessageFromControl() {
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) &&
          this.control.touched) {
        const lang: ILang = ValidationSupport.getValidatorErrorMessage(
            propertyName, this.control.errors[propertyName]);
        this.translate.get(lang.name, lang.values)
            .subscribe(errorMessage => {
              this.errorMessage = errorMessage;
              this.renderer.setElementProperty(
                  this.el.nativeElement, 'textContent', errorMessage);
            })
            .unsubscribe();
      }
    }
  }
  ngOnInit() {
    this.searchErrorMessageFromControl();
  }
  ngOnChanges() {
    this.ngif = !this.errorMessage ? true : null;
  }
}
