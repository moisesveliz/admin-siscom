import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs/Observable';

import {ILang} from '../../core/model/i-lang';
import {ValidationSupport} from '../../core/util/validation-support';

@Component({
  selector: 'app-control-messages',
  template:
      `<div *ngIf="errorMessage !== null" class="form-control-feedback">{{errorMessage | async}}</div>`
})
export class ControlMessagesComponent implements OnInit {
  @Input() control: FormControl;
  constructor(private translate: TranslateService) {}

  get errorMessage() {
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) &&
          this.control.touched) {
        const lang: ILang = ValidationSupport.getValidatorErrorMessage(
            propertyName, this.control.errors[propertyName]);
        return this.translate.get(lang.name, lang.values);
      }
    }
    return null;
  }

  ngOnInit() {}
}
