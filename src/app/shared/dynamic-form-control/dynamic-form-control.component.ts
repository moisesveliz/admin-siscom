import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {ControlBase} from '../../core/model/control-base';

@Component({
  selector: 'app-dynamic-form-control',
  templateUrl: './dynamic-form-control.component.html',
  styleUrls: ['./dynamic-form-control.component.css']
})
export class DynamicFormControlComponent implements OnInit {
  @Input() control: ControlBase<any>;
  @Input() form: FormGroup;

  constructor() {}

  ngOnInit() {}

  get isValid() {
    return this.form.controls[this.control.key].valid;
  }
}
