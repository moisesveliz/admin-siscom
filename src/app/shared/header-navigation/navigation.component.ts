import {AfterViewInit, Component} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateService} from '@ngx-translate/core';
import {Lang} from '../../core/model/lang';

@Component(
    {selector: 'app-navigation', templateUrl: './navigation.component.html'})
export class NavigationComponent implements AfterViewInit {
  name: string;
  showHide: boolean;
  langs: Array<Lang>;
  langSelected: Lang;

  constructor(private translate: TranslateService) {
    this.showHide = true;
    this.loadLangs();
    this.loadLangSelected();
  }

  changeShowStatus() {
    this.showHide = !this.showHide;
  }

  private loadLangSelected(): void {
    this.langSelected = this.findLang(this.translate.currentLang);
  }

  private loadLangs(): void {
    this.langs = this.translate.getLangs().map(lang => {
      return this.findLang(lang);
    });
  }

  changeLang(value): void {
    console.log('value: ' + value);
    this.translate.use(value);
    this.loadLangSelected();
  }

  private findLang(value): Lang {
    const lang: Lang = new Lang();
    lang.value = value;
    switch (value) {
      case 'es':
        lang.icon = 'flag-icon flag-icon-pe';
        lang.name = 'Español';
        break;
      case 'en':
        lang.icon = 'flag-icon flag-icon-us';
        lang.name = 'English';
        break;
      case 'fr':
        lang.icon = 'flag-icon flag-icon-fr';
        lang.name = 'French';
        break;
      default:
        break;
    }
    return lang;
  }


  ngAfterViewInit() {
    $(function() {
      $('.preloader').fadeOut();
    });

    const set = function() {
      const width =
          (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
      const topOffset = 0;
      if (width < 1170) {
        $('body').addClass('mini-sidebar');
        $('.navbar-brand span').hide();
        $('.sidebartoggler i').addClass('ti-menu');
      } else {
        $('body').removeClass('mini-sidebar');
        $('.navbar-brand span').show();
      }

      let height =
          ((window.innerHeight > 0) ? window.innerHeight : this.screen.height) -
          1;
      height = height - topOffset;
      if (height < 1) {
        height = 1;
      }
      if (height > topOffset) {
        $('.page-wrapper').css('min-height', (height) + 'px');
      }
    };
    $(window).ready(set);
    $(window).on('resize', set);

    $(document).on('click', '.mega-dropdown', function(e) {
      e.stopPropagation();
    });

    $('.search-box a, .search-box .app-search .srh-btn')
        .on('click', function() {
          $('.app-search').toggle(200);
        });

    (<any>$('.scroll-sidebar, .right-sidebar, .message-center'))
        .perfectScrollbar();

    $('body').trigger('resize');
  }
}
