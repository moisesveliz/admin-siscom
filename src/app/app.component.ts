import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(translate: TranslateService) {
    translate.addLangs(['es', 'en', 'fr']);
    // this language will be used as a fallback when a translation isn't found
    // in the current language
    translate.setDefaultLang('en');
    // the lang to use, if the lang isn't available, it will use the current
    // loader to get them
    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/es|en|fr/) ? browserLang : 'es');
  }
}
